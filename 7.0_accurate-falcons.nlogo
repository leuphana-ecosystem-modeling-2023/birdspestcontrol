; Nest-boxes and falconry: How does biological control due to pest feeding birds effect net-yield?
; @ David Jung (David.Jung@stud.leuphana.de)
; license
; copyright

directed-link-breed [ nest-links nest-link ] ; nest-species establish a link to the parent nest-box and provide it with "feeding" to hatch new individuals

breed [ good-birds good-bird ] ; predate on pest and NEAs
breed [ sparrow-birds sparrow-bird ]     ; predate like good-birds with increased intraguilt predation preferences
breed [ pest-birds pest-bird ] ; predate on crop
breed [ falcons falcon ]       ; predate on good-birds, pest-birds & rodents
breed [ rodents rodent ]       ; predate on crop and cause specific pest lowering "yield-potential" of patches
breed [ NEAs NEA]              ; predate only on artropod pest // most powerfull controllers of arthropod pest outbreaks
breed [ nest-boxes nest-box ]  ; hatch either tree sparrows (Passer montanus) or blue tits (Cyanistes caeruleus) and create links with the individuals

turtles-own [                  ; turtle variables that apply to all breeds above
  energy                       ; breeds gain (predation) and lose (movement) energy
  life-span                    ; value determines life span of different breeds // each tick takes away 1 life-span of turtles
  falcon-prey                  ; falcon-prey = 1 -> falcons predate // falcon-prey = 0 -> falcons don't predate
]

nest-boxes-own [ feeding ]     ; determines when new nest-species hatch

patches-own [                  ; patch variables that apply to all patches
  yield-potential              ; rises with decreased pest levels
  for-average-yield            ; for calculating average yield of entire simulation run (see procedure "average-yield")
  pest-level                   ;
  rodent-pest-level            ; } pest variables and yield potential are dependent on each other -> one goes up, the other goes down vice versa
  bird-pest-level              ;
]


;*****************************************************************;
;                     setup procedures                            ;
;*****************************************************************;

to setup
  clear-all
  setup-patches
  setup-pest-patches
  setup-pest-birds
  setup-rodents
  setup-NEAs
  setup-nest-boxes
  ifelse falconry? [
    setup-falcons] [
    ask falcons [die]
  ]
  reset-ticks
end

to setup-patches
  ask patches [
      set pcolor green
  ]
end

to setup-pest-patches ; every patch "rolls the dice" for being a pest patch or not
  ask patches [
    if random 100 < initial-infestation [ set pest-level 1 ] ; increased value of "initial-infestation" increases probability of each patch for becoming a pest patch
  ]
end

;******************************************************;
;             setup-nest-boxes                         ;
;******************************************************;

to setup-nest-boxes ; four different scenarios of nest-box availability for nesting bird species (blue tits & tree sparrows)
  if nest-box-density = "none" [ create-nest-boxes 0 ]

  if nest-box-density = "low" [ setup-low-density ]

  if nest-box-density = "medium" [setup-low-density setup-medium-density ]

  if nest-box-density = "high" [ setup-low-density setup-medium-density setup-high-density]

  ask nest-boxes [
    set shape "box"
    set color white
    set size 3
    set feeding feeding-for-hatch
    set life-span 10000 ; assuring their presence throughout the simulation
    set energy 10000 ; assuring their presence throughout the simulation
  ]
end

; every nest-box gets a fixed spot within the plot that depends on the distance to the edge of the interface plot
; this way location of nest-boxes is properly adapted when changing the plot size

to setup-low-density
  create-nest-boxes 1 [
    setxy ( 0.9 * max-pxcor ) ( 0 ) ] ; center to the right
  create-nest-boxes 1 [
    setxy ( -0.9 * max-pxcor ) ( 0 ) ] ; center to the left
  create-nest-boxes 1 [
    setxy ( 0 ) ( 0.9 * max-pycor ) ] ; upper center
  create-nest-boxes 1 [
    setxy ( 0 ) ( -0.9 * max-pycor ) ] ; lower center
end

to setup-medium-density
  create-nest-boxes 1 [
    setxy ( 0.9 * max-pxcor ) ( 0.9 * max-pycor ) ] ; upper right
  create-nest-boxes 1 [
    setxy ( -0.9 * max-pxcor ) ( 0.9 * max-pycor ) ] ; upper left
  create-nest-boxes 1 [
    setxy( 0.9 * max-pxcor ) ( -0.9 * max-pycor ) ] ; lower right
  create-nest-boxes 1 [
    setxy ( -0.9 * max-pxcor ) ( -0.9 * max-pycor ) ] ; lower left
end

to setup-high-density ; filling up the spaces in between
  create-nest-boxes 1 [
    setxy ( 0.9 * max-pxcor ) ( 0.45 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( 0.9 * max-pxcor ) ( -0.45 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( -0.9 * max-pxcor ) ( 0.45 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( -0.9 * max-pxcor ) ( -0.45 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( -0.45 * max-pxcor ) ( 0.9 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( 0.45 * max-pxcor ) ( 0.9 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( -0.45 * max-pxcor ) ( -0.9 * max-pycor ) ]
  create-nest-boxes 1 [
    setxy ( 0.45 * max-pxcor ) ( -0.9 * max-pycor ) ]
end

;********************************************************;
;                setup-turtles                           ;
;********************************************************:


to setup-pest-birds
  create-pest-birds n-pest-birds
  ask pest-birds [
    setxy random-xcor random-ycor
    set color 14
    set shape "bird"
    set size 2
    set life-span 200
    set energy 100
    set falcon-prey 1
  ]
end

to setup-falcons
  create-falcons n-falcons
  ask falcons [
    setxy random-xcor random-ycor
    set color 0
    set shape "hawk"
    set size 3.5
    set energy 10000
    set life-span 10000
    set falcon-prey 0
  ]
end

to setup-rodents
  create-rodents n-rodents
  ask rodents [
    setxy random-xcor random-ycor
    set color 31
    set shape "mouse side"
    set size 2
    set energy 80 + random-float 30
    set life-span 160 + random-float 40
    set falcon-prey 0 ; falcons preference for rodents is adressed in procedure "predation-falcons"
  ]
end

to setup-NEAs
  create-NEAs n-NEAs ; "n-NEAs" in a way represents fundamental resistance against pest outbreaks of the system // adjustment is cruial when changing plot size
  ask NEAs [
    setxy random-xcor random-ycor
    set color 135
    set shape "bug"
    set size 0.5
    set energy 100
    set life-span NEAs-life-span
    set falcon-prey 0
  ]
end

;**********************************************************;
;                    go procedures                         ;
;**********************************************************;

to go
 if ticks >=  1000 [ stop ]
  pest-growth                    ; arthropod pest has exponential growth if not checked
  potential-yield

  nest-boxes-hatch               ; hatching either "sparrow-birds" or "good-birds"
  nest-capacity                  ; carrying capacity of one nest-box is limited
  repopulate-nest                ; if nest-box doesn't hold birds it is repopulated after some time

  move-NEAs
  pred-NEAs                      ; main check for exponential growth of arthropod pest
  reprod-NEAs

  predation-nest-birds           ; specifying differences in intraguild predation between both breeds
  move-good-birds
  move-sparrow-birds

  move-pest-birds
  predation-pest-birds           ; specifying crop feeding and resulting individual pest levels
  reproduction-pest-birds        ;  MAYBE THROW THIS OUT

  move-rodents
  predation-rodents              ; specifying crop feeding and resulting individual pest dynamics
  reproduction-rodents           ; MAYBE THROW THIS OUT

  move-falcons
  predation-falcons              ; specifying predation preferences and patterns between falcons and prey breeds


  death                          ; to some breeds
  average-yield                  ; reports the final average-yield of each simulation run

  ifelse show-nest-links? [
    ask links [ set color blue ] ; links between birds and parent nests are visible when switch is "on"
  ] [
    ask links [ hide-link ] ; hiding the links when switch is "off"
  ]

  ask turtles [set life-span life-span - 1] ; each tick costs a turtle 1 "life-span"

  tick
end

;*****************************************************************;
;                       pest-procedures                           ;
;*****************************************************************;

to pest-growth
  ask patches [
    if pest-level >= 100 [ stop ]                   ; pest-level is manually checked (carrying capacity)
    if pest-level < 0 [ set pest-level 0 ]
    if pest-level > 0 [
      set pest-level (pest-level * pest-growth-rate ) ; "pest-growth-rate" gives the rate at which the pest-level of the individual patch increases (exponential if unchecked)
    ]
    if pest-level > pest-spread-rate [              ; "pest-spread-rate" detemines the threshold at which pest populations infest neighboring patches
      ask neighbors [ set pest-level pest-level + 0.5 ]
    ]
  if pest-level > 50 [ set pcolor red ]             ; for visualisation patches turn red when pest-level exceeds yield-potential
    if pest-level < 50 [ set pcolor green ]         ; and goes back to green vice versa
  ]
end

to potential-yield
  ask patches [
    set yield-potential (100 - ( pest-level + bird-pest-level + rodent-pest-level)) ;
  ]
end

;*****************************************************************;
;                  nest-box go procedures                         ;
;*****************************************************************;

to nest-boxes-hatch                                       ; two scenarios by choosing between one of two species that are hatched by nestboxes
  if nest-box-species = "Cyanistes caeruleus" [ nest-blue-tit ]
  if nest-box-species = "Passer montanus" [ nest-tree-sparrow ]
end

to nest-blue-tit
  ask nest-boxes [
    if feeding >= feeding-for-hatch [                     ; "feeding-for-hatch" gives the amount of "feeding" needed to hatch new individual
      hatch-good-birds 1 [ create-nest-link-to myself ]   ; each hatching process involves creation of a directed link between parent turtle (nest-box) and hatched turtle (bird)
      set feeding feeding - feeding-for-hatch
    ]
  ]
  ask good-birds [
    set color blue
    set shape "bird side"
    set size 2
    set energy 100
    set life-span 180 + random-float 30
    set falcon-prey 1
  ]
end

to nest-tree-sparrow
  ask nest-boxes [
    if feeding >= feeding-for-hatch [
      hatch-sparrow-birds 1 [ create-nest-link-to myself ]
      set feeding feeding - feeding-for-hatch
    ]
  ]
  ask sparrow-birds [
    set color brown
    set shape "bird side"
    set size 2
    set energy 100
    set life-span 180 + random-float 30
    set falcon-prey 1
  ]
end

to nest-capacity
    ask nest-boxes [
      if count link-neighbors > 4 [
      ask one-of link-neighbors [ die ]                     ; if a nest-box has 4 birds attached to it the carrying capacity is reached
    ]
  ]
end

to repopulate-nest
  ask nest-boxes [
    if not any? link-neighbors [
      ask nest-boxes with [not any? link-neighbors] [       ; if a nest-box has no birds attached to it repopulation occurs
        set feeding feeding + repopulation-rate             ; time-delay of repopulation process -> the higher the tick-based increment ("repopulation-rate") the shorter the time delay
        nest-boxes-hatch                                    ; calling the hatch procedure from above
      ]
    ]
  ]
end

;*****************************************************************;
;                  Natural Enemy Arthropods                       ;
;*****************************************************************;

to move-NEAs                                               ; NEAs move around the plot randomly until they find food
  ask NEAs [
    left random 360
    forward 0.5
    set energy energy - 1
  ]
end

to pred-NEAs                                               ; NEAs find food -> they realize pest-levels around them at a certain level and distance
  ask NEAs [
      if any? (patches with [pest-level > 20 and distance myself < 3]) [
        move-to one-of patches with [ pest-level > 20 and distance myself < 3]
      set energy energy + NEAs-energy-from-pest            ; slider for NEAs-energy-from-pest
      ]
    if any? NEAs-on patches with [pest-level > 20] [
    set pest-level pest-level - NEA-pest-consumption       ; slider for NEA-pest-consumption -> how much of the pest-level is taken away?
      if pest-level < 0 [set pest-level 0]
    ]
  ]
end

to reprod-NEAs
  if count NEAs < n-NEAs [ setup-NEAs ]                    ; making sure they have a constant minimum abundance
  ask NEAs [
    if energy > NEAs-energy-for-reproduction [
      set energy energy - NEAs-energy-for-reproduction
      hatch 1 [                                            ; slider NEAs-energy-for-reproduction gives the rate at which this breed reproduces
        set energy NEAs-energy-for-reproduction
        set life-span NEAs-life-span                       ; slider for NEAs life-span account
      ]
    ]
  ]
end


;***********************************************************************;
;                            Nest-species procedures                    ;
;***********************************************************************;


to predation-nest-birds
  if nest-box-species = "Cyanistes caeruleus" [ predation-good-birds ]
  if nest-box-species = "Passer montanus" [ predation-sparrow-birds ]
end

to move-good-birds
  ask good-birds [
    wiggle
    fd 1
    set energy energy - 3
  ]
end

to predation-good-birds
  ask good-birds [

    if (any? NEAs-here and not any? patches with [pest-level > 25]) [
      ask one-of NEAs-here [ die ]
      set energy energy + 7
      ask link-neighbors [ set feeding feeding + 7 ]
    ]

      if (any? patches with [pest-level > 25] and not any? NEAs-here and random-float 100 < 25) [ ; every fourth patch with this condition is preyed upon
        ask one-of patches with [pest-level > 25] [
          set pest-level pest-level - 6 ]
        set energy energy + 1
        ask link-neighbors [ set feeding feeding + 2 ]
    ]

      if (any? NEAs-here and any? patches with [pest-level > 25]) [
        ifelse random 100 < 50 [ ; aspect of intraguilt predation
          ask one-of NEAs-here [ die ]
          set energy energy + 7
          ask link-neighbors [ set feeding feeding + 2 ]
      ]
      [ ask one-of patches with [pest-level > 25] [
        set pest-level pest-level - 6]
        set energy energy + 1
        ask link-neighbors [ set feeding feeding + 2 ]
      ]
    ]
  ]
end


to move-sparrow-birds
  ask sparrow-birds [
    wiggle
    fd 1
    set energy energy - 3
  ]
end

to predation-sparrow-birds
  ask sparrow-birds [

    if (any? NEAs-here and not any? patches with [pest-level > 25]) [
      ask one-of NEAs-here [ die ]
      set energy energy + 5
      ask link-neighbors [ set feeding feeding + 5 ]
    ]

      if (any? patches with [pest-level > 25] and not any? NEAs-here) [
        ask one-of patches with [pest-level > 15] [
          set pest-level pest-level - 6 ]
        set energy energy + 1
        ask link-neighbors [ set feeding feeding + 1 ]
    ]

      if (any? NEAs-here and any? patches with [pest-level > 25]) [
        ifelse random 100 < 90 [ ; higher propability of intraguilt predation than good-birds
          ask one-of NEAs-here [ die ]
          set energy energy + 5
          ask link-neighbors [ set feeding feeding + 5 ]
      ]
      [ ask one-of patches with [pest-level > 25] [
        set pest-level pest-level - 6]
        set energy energy + 1
        ask link-neighbors [ set feeding feeding + 1 ]
      ]
    ]
  ]
end


;********************************************************************;
;                     pest-bird procedures                           ;
;********************************************************************;

to move-pest-birds
  ask pest-birds [
    wiggle
    fd 1
    set energy energy - 1
  ]
end

to predation-pest-birds
  ask pest-birds [
    if ( any? patches with [yield-potential > 5])[
      ask one-of patches with [yield-potential > 5]
      [set bird-pest-level bird-pest-level + 10]
    ]
  ]
end

to reproduction-pest-birds
  if count pest-birds < 1 [ setup-pest-birds ] ; making sure they dont become extinct
  ask pest-birds [
    if energy > 200 [ ; slider for reproduction rate
      set energy energy - 200
      hatch 1 [
        set energy 100
        set life-span 200 + random-float 30
      ]
    ]
  ]
end

;********************************************************************;
;                        rodents procedures                          ;
;********************************************************************;

to move-rodents
  ask rodents [
    wiggle
    fd 1
    set energy energy - 1
  ]
end

to predation-rodents
  ask rodents [
    if any? patches with [yield-potential > 0] [
      ask one-of patches with [yield-potential > 0 ] [
        set rodent-pest-level rodent-pest-level + 10 ]
      set energy energy + 1
    ]
  ]
end

to reproduction-rodents
  if count rodents < 1 [ setup-rodents ] ; making sure they dont become extinct
  ask rodents [
    if energy > 200 [
    set energy energy - 200
    hatch 1 [
      set energy 200
      set life-span 200
      ]
    ]
  ]
end

;*************************************************************************;
;                          falcon procedures                              ;
;*************************************************************************;

to move-falcons
  ask falcons [
    wiggle
    fd 1.5
  ]
end
; falcons don't lose energy when moving because their populations are anthropogenically controlled due to falconry (see info: "falconry")

to predation-falcons
  ask falcons [
    if (any? turtles with [ falcon-prey = 1  and distance myself < 4]) [
      ask (one-of turtles with [ falcon-prey = 1 and distance myself < 4]) [
        die
      ]
    ]
     if any? (rodents with [distance myself < 15 ]) [
      set heading towards one-of rodents with [ distance myself < 15 ] fd 2
    ]
    if any? rodents-here [
      ask rodents-here [die]
    ]
  ]
end


; falcons don't gain energy for the same reason they don't lose any

;***********************************************************************;
;                        general procedures                             ;
;***********************************************************************;

to wiggle
  rt random 90
  lt random 90
end

to display-labels
  ask turtles [ set label ""]
  if show-feeding? [
    ask nest-boxes [ set label round feeding ]
  ]
end

to death
  ask turtles [
    if energy <= 0 [die]
    if life-span <= 0 [die]
  ]
end

to average-yield
  ask patches [
    set for-average-yield (for-average-yield + yield-potential) / 1 + ticks
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
235
45
731
462
-1
-1
8.0
1
10
1
1
1
0
1
1
1
-30
30
-25
25
1
1
1
ticks
30.0

BUTTON
5
45
104
115
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
122
45
221
115
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

PLOT
735
210
1074
375
Pest-Yield Balance
time
totals
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"yield-potential" 1.0 0 -13840069 true "" "plot mean [ yield-potential ] of patches"
"arthropod-pest" 1.0 0 -2674135 true "" "plot mean [ pest-level ] of patches"
"bird-pest" 1.0 0 -10402772 true "" "plot mean [ bird-pest-level ] of patches"
"rodent-pest" 1.0 0 -5207188 true "" "plot mean [ rodent-pest-level ] of patches"

SLIDER
1101
444
1294
477
n-pest-birds
n-pest-birds
0
100
9.0
1
1
NIL
HORIZONTAL

SLIDER
6
186
221
219
n-falcons
n-falcons
0
20
3.0
1
1
NIL
HORIZONTAL

SLIDER
1101
407
1293
440
n-rodents
n-rodents
0
50
9.0
1
1
NIL
HORIZONTAL

SLIDER
1098
223
1289
256
n-NEAs
n-NEAs
0
200
11.0
1
1
NIL
HORIZONTAL

PLOT
735
46
1072
208
Populations
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"NEAs * 10²" 1.0 0 -1264960 true "" "plot (count NEAs) / 100"
"pest-level" 1.0 0 -2674135 true "" "plot  mean [ pest-level ] of patches "
"good-birds" 1.0 0 -13791810 true "" "plot (count good-birds)"

SLIDER
1098
81
1285
114
NEA-pest-consumption
NEA-pest-consumption
10
30
19.0
1
1
NIL
HORIZONTAL

SLIDER
1100
281
1273
314
pest-growth-rate
pest-growth-rate
1.0
2.0
1.1
0.1
1
NIL
HORIZONTAL

SLIDER
1101
317
1274
350
pest-spread-rate
pest-spread-rate
15
40
35.0
5
1
NIL
HORIZONTAL

SLIDER
1098
116
1286
149
NEAs-energy-from-pest
NEAs-energy-from-pest
1
5
2.0
0.1
1
NIL
HORIZONTAL

SLIDER
1098
151
1287
184
NEAs-energy-for-reproduction
NEAs-energy-for-reproduction
10
200
40.0
10
1
NIL
HORIZONTAL

SLIDER
1098
187
1288
220
NEAs-life-span
NEAs-life-span
10
100
60.0
5
1
NIL
HORIZONTAL

SWITCH
6
150
221
183
falconry?
falconry?
1
1
-1000

CHOOSER
3
241
223
286
nest-box-density
nest-box-density
"none" "low" "medium" "high"
3

CHOOSER
3
290
223
335
nest-box-species
nest-box-species
"Cyanistes caeruleus" "Passer montanus"
0

SLIDER
3
339
223
372
feeding-for-hatch
feeding-for-hatch
50
200
183.0
1
1
NIL
HORIZONTAL

TEXTBOX
10
10
1113
48
Nest-boxes and falconry: How does biological control due to pest feeding birds effect net-yield? 
25
0.0
1

TEXTBOX
1102
50
1252
78
Natural enemy arthropod (NEA) parameters
11
0.0
1

TEXTBOX
1100
263
1250
281
Arthropod pest parameters
11
0.0
1

TEXTBOX
70
122
144
140
Treatments
14
0.0
1

TEXTBOX
8
136
158
154
falconry
11
0.0
1

TEXTBOX
6
228
156
246
nest-boxes
11
0.0
1

TEXTBOX
1102
391
1287
419
Non-arthropod pest parameters
11
0.0
1

SWITCH
4
415
140
448
show-feeding?
show-feeding?
0
1
-1000

MONITOR
986
379
1074
424
average yield
mean [for-average-yield] of patches
2
1
11

SLIDER
1101
353
1275
386
initial-infestation
initial-infestation
0
10
2.0
1
1
NIL
HORIZONTAL

SWITCH
4
451
151
484
show-nest-links?
show-nest-links?
1
1
-1000

SLIDER
3
376
223
409
repopulation-rate
repopulation-rate
0
10
2.0
1
1
NIL
HORIZONTAL

@#$#@#$#@
## Notes 4 David

turtles: falcons, benefitial birds, pest birds, natural enemy arthropods (NEA), rodents
patches attributes: potential yield <->  pest infestation (balance)

falcons: costfull reproduction, high mobility and low abundance (prey = benefitial/pest birds), life-cycle (longevity)

benefitial and pest birds: medium reproduciton, high mobility, medium abundance ( prey = pest form patches, NEA (predation preferences differ between the two groups), life-cycle

NEA: exponential growth (if unchecked; checks: birds, natural phenomena), short life-cycle, 

dat-and-time for spawning breeds at specific times

nest-boxes? look in the ants model

exclude life-span? ecological niche concept: empty space would be filled with new individuals from outside the research area?


Balance between NEAs and Pest-level crucial calibration for further predator-prey introductions

Net-Yield: added up yield-potential of every tick

After feedback:

Formating Netlogo data to csv files

negative values for populations?

adding legend for agentsets in interface

NEAs turn into patch properties at certain value and become turtles again at certain level



## WHAT IS IT?

Model of avian predation in agriculture for arthropod pests of strawberries

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

bird
false
0
Polygon -7500403 true true 135 165 90 270 120 300 180 300 210 270 165 165
Rectangle -7500403 true true 120 105 180 237
Polygon -7500403 true true 135 105 120 75 105 45 121 6 167 8 207 25 257 46 180 75 165 105
Circle -16777216 true false 128 21 42
Polygon -7500403 true true 163 116 194 92 212 86 230 86 250 90 265 98 279 111 290 126 296 143 298 158 298 166 296 183 286 204 272 219 259 227 235 240 241 223 250 207 251 192 245 180 232 168 216 162 200 162 186 166 175 173 171 180
Polygon -7500403 true true 137 116 106 92 88 86 70 86 50 90 35 98 21 111 10 126 4 143 2 158 2 166 4 183 14 204 28 219 41 227 65 240 59 223 50 207 49 192 55 180 68 168 84 162 100 162 114 166 125 173 129 180

bird side
false
0
Polygon -7500403 true true 0 120 45 90 75 90 105 120 150 120 240 135 285 120 285 135 300 150 240 150 195 165 255 195 210 195 150 210 90 195 60 180 45 135
Circle -16777216 true false 38 98 14

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

hawk
true
0
Polygon -7500403 true true 151 170 136 170 123 229 143 244 156 244 179 229 166 170
Polygon -16777216 true false 152 154 137 154 125 213 140 229 159 229 179 214 167 154
Polygon -7500403 true true 151 140 136 140 126 202 139 214 159 214 176 200 166 140
Polygon -16777216 true false 151 125 134 124 128 188 140 198 161 197 174 188 166 125
Polygon -7500403 true true 152 86 227 72 286 97 272 101 294 117 276 118 287 131 270 131 278 141 264 138 267 145 228 150 153 147
Polygon -7500403 true true 160 74 159 61 149 54 130 53 139 62 133 81 127 113 129 149 134 177 150 206 168 179 172 147 169 111
Circle -16777216 true false 144 55 7
Polygon -16777216 true false 129 53 135 58 139 54
Polygon -7500403 true true 148 86 73 72 14 97 28 101 6 117 24 118 13 131 30 131 22 141 36 138 33 145 72 150 147 147

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

mouse side
false
0
Polygon -7500403 true true 38 162 24 165 19 174 22 192 47 213 90 225 135 230 161 240 178 262 150 246 117 238 73 232 36 220 11 196 7 171 15 153 37 146 46 145
Polygon -7500403 true true 289 142 271 165 237 164 217 185 235 192 254 192 259 199 245 200 248 203 226 199 200 194 155 195 122 185 84 187 91 195 82 192 83 201 72 190 67 199 62 185 46 183 36 165 40 134 57 115 74 106 60 109 90 97 112 94 92 93 130 86 154 88 134 81 183 90 197 94 183 86 212 95 211 88 224 83 235 88 248 97 246 90 257 107 255 97 270 120
Polygon -16777216 true false 234 100 220 96 210 100 214 111 228 116 239 115
Circle -16777216 true false 246 117 20
Line -7500403 true 270 153 282 174
Line -7500403 true 272 153 255 173
Line -7500403 true 269 156 268 177

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.3.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="1.0 boxes_and_falcons" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1000"/>
    <metric>count turtles</metric>
    <metric>mean [yield-potential] of patches</metric>
    <metric>mean [pest-level] of patches</metric>
    <metric>mean [bird-pest-level] of patches</metric>
    <metric>mean [rodent-pest-level] of patches</metric>
    <enumeratedValueSet variable="NEAs-life-span">
      <value value="60"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="falconry?">
      <value value="false"/>
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-for-reproduction">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-feeding?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-density">
      <value value="&quot;high&quot;"/>
      <value value="&quot;none&quot;"/>
      <value value="&quot;low&quot;"/>
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-falcons">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-pest-birds">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-growth-rate">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-infestation">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-species">
      <value value="&quot;Cyanistes caeruleus&quot;"/>
      <value value="&quot;Passer montanus&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-spread-rate">
      <value value="35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feeding-for-hatch">
      <value value="183"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-nest-links?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="repopulation-rate">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-rodents">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-from-pest">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEA-pest-consumption">
      <value value="19"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NEAs">
      <value value="11"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="1.1 boxes_and_falcons" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="50"/>
    <metric>count turtles</metric>
    <metric>mean [ precision yield-potential 2] of patches</metric>
    <metric>mean [precision pest-level 2] of patches</metric>
    <metric>mean [ precision bird-pest-level 2] of patches</metric>
    <metric>mean [ precision rodent-pest-level 2] of patches</metric>
    <enumeratedValueSet variable="NEAs-life-span">
      <value value="60"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="falconry?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-for-reproduction">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-feeding?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-density">
      <value value="&quot;high&quot;"/>
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-falcons">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-pest-birds">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-growth-rate">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-infestation">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-species">
      <value value="&quot;Cyanistes caeruleus&quot;"/>
      <value value="&quot;Passer montanus&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-spread-rate">
      <value value="35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feeding-for-hatch">
      <value value="183"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-nest-links?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="repopulation-rate">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-rodents">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-from-pest">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEA-pest-consumption">
      <value value="19"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NEAs">
      <value value="11"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="1.3 boxes_and_falcons" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1000"/>
    <metric>mean [yield-potential] of patches</metric>
    <metric>mean [pest-level] of patches</metric>
    <metric>mean [bird-pest-level] of patches</metric>
    <metric>mean [rodent-pest-level] of patches</metric>
    <enumeratedValueSet variable="NEAs-life-span">
      <value value="60"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="falconry?">
      <value value="false"/>
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-for-reproduction">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-feeding?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-density">
      <value value="&quot;high&quot;"/>
      <value value="&quot;none&quot;"/>
      <value value="&quot;low&quot;"/>
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-falcons">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-pest-birds">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-growth-rate">
      <value value="1.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-infestation">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nest-box-species">
      <value value="&quot;Cyanistes caeruleus&quot;"/>
      <value value="&quot;Passer montanus&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pest-spread-rate">
      <value value="35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="feeding-for-hatch">
      <value value="183"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-nest-links?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="repopulation-rate">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-rodents">
      <value value="9"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEAs-energy-from-pest">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="NEA-pest-consumption">
      <value value="19"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="n-NEAs">
      <value value="11"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
